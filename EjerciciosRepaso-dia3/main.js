/**
 * ###################################
 * ###### E J E R C I C I O   1 ######
 * ###################################
 *
 * Sara y Laura juegan al baloncesto en diferentes equipos. En los
 * últimos 3 partidos, el equipo de Sara anotó 89, 120 y 103 puntos,
 * mientras que el equipo de Laura anotó 116, 94, y 123 puntos.
 *
 * `1.` Calcula la media de puntos para cada equipo.
 *
 * `2.` Muestra un mensaje que indique cuál de los dos equipos
 *      tiene mejor puntuación media. Incluye en este mismo mensaje
 *      la media de los dos equipos.
 *
 * `3.` María también juega en un equipo de baloncesto. Su equipo
 *      anotó 97, 134 y 105 puntos respectivamente en los últimos
 *      3 partidos. Repite los pasos 1 y 2 incorporando al equipo
 *      de María.
 *
 */

const equipoDeSara = {
    leader: 'Sara',
    scores: [89, 120, 103]
};

const equipoDeLaura = {
    leader: 'Laura',
    scores: [116, 94, 123]
};

const equipoDeMaria = {
    leader: 'Maria',
    scores: [97, 134, 105]
};

const average = (arrayOfValues) => {
    let sumOfAllValues = arrayOfValues.reduce((accumulator, value) => {
        return accumulator + value;
    });

    return sumOfAllValues / 2;
}

function getBestScoredTeam(arrayOfTeams) {
    let teamWinning = arrayOfTeams[0];
    for (let i = 1; i < arrayOfTeams.length; i++) {
        if (average(arrayOfTeams[i].scores) > average(teamWinning.scores)) {
            teamWinning = arrayOfTeams[i];
        }
    }

    return teamWinning;
}

function sayWinner(arrayOfTeams) {
    const winner = getBestScoredTeam(arrayOfTeams);
    console.log(`El equipo de ${winner.leader} es el ganador.`);
}

const arrayOfTeams = [equipoDeLaura, equipoDeMaria, equipoDeSara];
sayWinner(arrayOfTeams);

/**
 * ###################################
 * ###### E J E R C I C I O   2 ######
 * ###################################
 *
 * Jorge y su familia han ido a comer a tres restaurantes distintos.
 * La factura fue de 124€, 58€ y 268€ respectivamente.
 *
 * Para calcular la propina que va a dejar al camarero, Jorge ha
 * decidido crear un sistema de calculo (una función). Quiere
 * dejar un 20% de propina si la factura es menor que 50€, un 15%
 * si la factura está entre 50€ y 200€, y un 10% si la factura es
 * mayor que 200€.
 *
 * Al final, Jorge tendrá dos arrays:
 *
 * - `Array 1` Contiene las propinas que ha dejado en cada uno de
 *    los tres restaurantes.
 *
 * - `Array 2` Contiene el total de lo que ha pagado en cada uno de
 *    los restaurantes (sumando la propina).
 *
 * `NOTA` Para calcular el 20% de un valor, simplemente multiplica
 *  por `0.2`. Este resultado se obtiene de dividir `20/100`. Si
 *  quisieramos averiguar el 25% de un valor lo multiplicaríamos
 *  por 0.25.
 *
 * `25 / 100 = 0.25`.
 *
 */


class Gastos {
    constructor() {
        this.propinas = [];
        this.gastosTotales = [];
    }

    calcularPropina(factura) {
        let propina = 0;
        if (factura < 50) {
            propina = (factura * 0.2).toFixed(2);
        } else if (factura > 200) {
            propina = (factura * 0.1).toFixed(2);
        } else {
            propina = (factura * 0.15).toFixed(2);
        }

        this.addPropina(propina);
        this.addGasto(factura, propina);

        return propina;
    }

    addPropina(propina) {
        this.propinas.push(propina);
    }

    addGasto(factura, propina) {
        let pagoTotal = Number(factura) + Number(propina);
        this.gastosTotales.push(pagoTotal);
    }

    get arrayPropinas() {
        return this.propinas;
    }

    get arrayGastosTotales() {
        return this.gastosTotales;
    }
}

const misGastos = new Gastos();
const facturas = [124, 58, 268];

for (let factura of facturas) {
    console.log(`propina de ${factura}:`, misGastos.calcularPropina(factura));
}

console.log('Propinas:', misGastos.arrayPropinas, 'Gastos Totales:', misGastos.arrayGastosTotales);

/**
* ###################################
* ###### E J E R C I C I O   3 ######
* ###################################
*
* Dado el siguiente array de números:
*
* `nums = [100, 3, 4, 2, 10, 4, 1, 10]`
*
* `1.` Recorre todo el array y muestra por consola cada uno de sus
*      elementos con la ayuda de un `for`, con la ayuda de un `map`
*      y con la ayuda de un `for...of`.
*
* `2.` Ordena el array de menor a mayor sin emplear `sort()`.
*
* `3.` Ordena el array de mayor a menor empleando `sort()`.
*
*/

nums = [100, 3, 4, 2, 10, 4, 1, 10];

//--> 1
console.log('array recorrida con un for:');

for (let index = 0; index < nums.length; index++) {
    console.log(nums[index]);
}

console.log('array recorrida con un map:');

nums.map((num) => {
    console.log(num);
});

console.log('array recorrida con un for of:');

for (let num of nums) {
    console.log(num);
}

/**
* ###################################
* ###### E J E R C I C I O   4 ######
* ###################################
*
* Crea una `arrow function` que reciba dos números por medio del
* `prompt`, reste ambos números, y nos devuelva el resultado.
* En caso de que el resultado sea negativo debe cambiarse a
* positivo. Este resultado se mostrará por medio de un `alert`.
*
*/

// const num1 = prompt('insert one number:');
// const num2 = prompt('insert another number:');

// const differenceBetweenNumbers = (num1, num2) => {
//     const difference = Math.abs(num1 - num2);
//     alert(`The difference between ${num1} and ${num2} is: ${difference}`);
// };

// differenceBetweenNumbers(num1, num2);


// EJERCICIO DEL DIA
class Banco {
    constructor(nombre, direccion) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.clientes = [];
    }

    newClient(nameOfClient, idOfCuenta) {
        this.clientes.push(new Client(nameOfClient, idOfCuenta));
    }

    get arrayClientes() {
        return this.clientes;
    }
}

class Client {
    constructor(name, idCuenta) {
        this.name = name;
        this.idCuenta = idCuenta;
    }
}

class CuentaBancaria {
    constructor(id) {
        this.balance = 0;
        this.id = id;
    }
}

class Titular {
    constructor(nombre, genero, monedero) {
        this.nombre = nombre;
        this.genero = genero;
        this.monedero = monedero;
        this.id = this.generateID();
        this.cuentasBancarias = [];
    }

    generateID() {
        return Math.floor(Math.random() * 999999999);
    }

    abrirCuentaBancaria(banco) {
        // 1. Generar un ID para la nueva cuenta bancaria.
        const idCuentaBancaria = this.generateID();

        // 2. Acceder al array "clientes" del banco y almacenar el 
        //    nombre y el ID de la cuenta bancaria del nuevo cliente
        banco.newClient(this.nombre, idCuentaBancaria); //><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        // 3. Crear una nueva cuenta bancaria a la que debemos pasar
        //    el nuevo ID.
        this.cuentasBancarias.push(new CuentaBancaria(idCuentaBancaria)); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    }

    ingresarDinero(cantidad, cuenta) {
        // 1. Comprobar si en el monedero tenemos la cantidad que
        //    deseamos ingresar. De no ser asÃ­, mostrar un mensaje
        //    que diga que no tenemos suficiente dinero en el 
        //    monedero.
        if (cantidad > this.monedero) {
            console.warn('Not enough money man!');
            return;
        }
        // 2. Si tenemos suficiente dinero en el monedero, solo
        //    queda restar en el monedero la cantidad que vamos
        //    a ingresar.
        this.monedero -= cantidad;
        // 3. Acceder a la propiedad "balance" de la cuenta bancaria
        //    y sumar la cantidad a ingresar. Mostrar un mensaje de
        //    que el ingreso ha sido realizado.
        cuenta.balance += cantidad;
    }

    retirarDinero(cantidad, cuenta) {
        // 1. Comprobar si en la propiedad "balance" de nuestra
        //    cuenta tenemos la cantidad que deseamos retirar. 
        //    De no ser asÃ­, mostrar un mensaje que diga que
        //    no tenemos suficiente dinero en la cuenta.
        if (this.checkAuthenticityAccount(cuenta)) {
            if (cuenta.balance > cantidad) {
                cuenta.balance -= cantidad;
                this.monedero += cantidad;
                console.log(`La cantidad de ${cantidad} ha sido retirada con éxito.`);
            }
        } else {
            console.log('Cuenta no existente.');
        }
        // 2. Si tenemos suficiente dinero en la cuenta, solo
        //    queda restar en el balance la cantidad que vamos
        //    a retirar.

        // 3. Acceder a la propiedad "monedero" del titular
        //    y sumar la cantidad retirada al monedero. Mostrar 
        //    un mensaje de que el ingreso ha sido realizado.
    }

    checkAuthenticityAccount(cuenta) {
        for (let cuentaBancaria of this.cuentasBancarias) {
            if (cuentaBancaria === cuenta) {
                return true;
            }
        }

        return false;
    }

    mostrarSaldo(cuenta) {
        // 1. Acceder a la propiedad "balance" de la cuenta y
        //    mostrar un mensaje que nos indique nuestro saldo
        //    actual.
        if (this.checkAuthenticityAccount(cuenta)) {
            console.log(`Tu saldo actual es de ${cuenta.balance}.`);
        } else {
            console.log('Cuenta no existente.');
        }
    }


}

const cajaRural = new Banco('Caja Rural', 'calle Fausto Vigil');
const manuel = new Titular('Manuel', 'Hombre', 1200);

//miramos los clientes que tiene el banco:
console.log('clientes iniciales caja rural:', cajaRural.arrayClientes);

//miramos las cuentas bancarias de manuel:
console.log('cuentas bancarias iniciales de manuel:', manuel.cuentasBancarias);

//-----> creamos una nueva cuenta bancaria:
manuel.abrirCuentaBancaria(cajaRural);

//miramos los clientes que tiene el banco:
console.log('clientes caja rural:', cajaRural.arrayClientes);

//miramos las cuentas bancarias de manuel:
console.log('cuentas bancarias de manuel:', manuel.cuentasBancarias);

//-----> comprobamos el monedero:
console.log('monedero de manuel:', manuel.monedero);

//-----> ingresamos dinero:
manuel.ingresarDinero(1000, manuel.cuentasBancarias[0]);

//-----> comprobamos el ingreso:
manuel.mostrarSaldo(manuel.cuentasBancarias[0]);

//-----> comprobamos el monedero:
console.log('monedero de manuel:', manuel.monedero);

//-----> retiramos dinero:
manuel.retirarDinero(200, manuel.cuentasBancarias[0]);

//-----> comprobamos el retiro:
manuel.mostrarSaldo(manuel.cuentasBancarias[0]);

//-----> comprobamos el monedero:
console.log('monedero de manuel:', manuel.monedero);

// EJERCICIOS EXTRA
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function (s, t) {
    const sToCharArray = s.split('');
    const tToCharArray = t.split('');

    for (let index = 0; index < s.length; index++) {
        if (sToCharArray[index] !== tToCharArray[(s.length - 1) - index]) {
            return false;
        }
    }

    return true;
};

const s = 'aitana', t = 'anatia';
console.log(`is ${t} the anagram of ${s}?:`, isAnagram(s, t));


/**
 * Given an array of numbers, ONLY ONE NUMBER IS REPEATED.
 * @param {number[]} nums
 * @return {number} the repeated number
 */
var repeatedNTimes = function (nums) {
    let possibleNums = [];

    for (let num of nums) {
        for (let num2 of possibleNums) {
            if (num === num2) {
                return num;
            }
        }

        possibleNums.push(num);
    }
};

const arrayOfNumbers = [5, 1, 5, 2, 5, 3, 5, 4];
console.log(`repeated num in array [${arrayOfNumbers}]:`, repeatedNTimes(arrayOfNumbers));


/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortArrayByParity = function (nums) {
    const evenNums = [];
    const oddNums = [];

    for (let num of nums) {
        if (num % 2 === 0) {
            evenNums.push(num);
        } else {
            oddNums.push(num);
        }
    }

    return evenNums.concat(oddNums);
};

const otherArrayOfNumbers = [1, 4, 2, 6, 3, 7, 8, 9];
console.log(`sort [${otherArrayOfNumbers}] by parity:`, sortArrayByParity(otherArrayOfNumbers));

const anotherArrayOfNumbers = [3, 1, 2, 4];
console.log(`sort [${anotherArrayOfNumbers}] by parity:`, sortArrayByParity(anotherArrayOfNumbers));
