/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function (nums) {
    /*El primer for recorre la array de numeros de 
    principio a fin*/
    for (let i = 0; i < nums.length; i++) {
        /*el segundo for recorre la array desde la posicion
        siguiente a la i (es decir, i+1), para ir comprobando
        si alguno de los demas numeros de la array es igual al de
        la posicion i en la que estamos.*/
        for (let j = i + 1; j < nums.length; j++) {
            if (nums[i] === nums[j]) {
                return true; //si es igual devolvemos true
            }
        }
    }
    /*si el (primer) for termina, eso quiere decir que hemos recorrido
        toda la array y no se ha encontrado ningun numero igual a otro. 
        Por lo tanto, devolvemos false (= 'no hay duplicados')*/
    return false;
};

/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaxConsecutiveOnes = function (nums) {
    let counterOfOnes = 0; /*esta variable ira 'contando'
    todos los unos consecutivos que nos iremos encontrando.*/
    let maxNumOnes = 0; /*aqui almacenaremos el max num de unos 
    consecutivos alcanzado hasta el momento.*/

    //este for se encarga de recorrer toda la array de numeros.
    for (let i = 0; i < nums.length; i++) {
        /*si el numero en la posicion en la que estamos (i)
        es igual a 1:*/
        if (nums[i] === 1) {
            counterOfOnes++; //contamos un uno mas (aumentamos la variable).

            /*si el numero max de unos almacenado es menor que el
            conseguido hasta el momento, modificamos la variable.*/
            if (maxNumOnes < counterOfOnes) {
                maxNumOnes = counterOfOnes;
            }

            /*si el numero en la posicion en la que estamos (i)
            NO es igual a 1:*/
        } else {
            counterOfOnes = 0; /*reestablecemos el contador a 0 porque se 
            rompe la cadena.*/
        }
    }
    /*al final, tras recorrer toda la array, devolvemos el numero maximo de unos
    que se llego a alcanzar:*/
    return maxNumOnes;
};

/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function (nums) {
    let i = 0; //index para recorrer la array.
    let counterOfZeroes = 0; /*aqui almacenaremos el numero de ceros que nos iremos
    encontrando al recorrer la array de nums.*/

    /*mientras que la i (index) no supere el tamaño de la array de nums (sin contar 
        los ceros que seran desplazados al final)*/
    while (i < nums.length - counterOfZeroes) {
        //si el valor de la array en la posicion i es un cero:
        if (nums[i] === 0) {
            let storeNum = nums[i]; //almacenamos ese valor en una variable provisional.
            nums.splice(i, 1); //eliminamos el valor de la posicion en la que esta (i)
            nums.push(storeNum); /*lo introducimos de nuevo al final de la array de nums 
            (usando el almacenado).*/

            counterOfZeroes++; //'contamos' un cero encontrado (aumentamos la variable)

            /*NOTA: no nos desplazamos a la siguiente posicion de la array (no aumentamos i),
            ya que al eliminar un elemento hemos desplazado todos los elementos hacia la izq
            de la array y por tanto, el valor que está ahora en la posicion i (donde estamos)
            ES EL SIQUIENTE VALOR (y debemos comprobarlo).*/

            //si el valor de la array en la posicion i NO es un cero:
        } else {
            i++; //nos desplazamos a la siquiente posicion de la array.
        }
    }
    /*PD: la razon por la que hice un contador de ceros es para que cuando el bucle 
    llegue al final de la array (tras haber comprobado el ultimo numero) y se encuentre con todos 
    los ceros que hemos desplazado NO SIGA DESPLAZANDOLOS HASTA LA ETERNIDAD*/

    //una vez recorrida toda la array, la devolvemos con los cambios:
    return nums;
};

const array = [0, 0, 0, 0, 1, 0, 3, 0, 0, 1, 12, 0, 30, 2, 0, 0, 1];
console.log('contains duplicate?:', containsDuplicate(array));

const binaryArray = [1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0];
console.log(`maximum consecutive ones in [${binaryArray}]:`, findMaxConsecutiveOnes(binaryArray));

console.log('move zeroes:', moveZeroes(array));



// EJERCICIOS EXTRAS //
/**
 * @param {number[][]} image
 * @param {number} sr
 * @param {number} sc
 * @param {number} newColor
 * @return {number[][]}
 */
var floodFill = function (image, sr, sc, newColor) {
    //guardamos el color original de la posicion
    const initColor = image[sr][sc];

    //cambiamos el color de la posicion por el nuevo
    image[sr][sc] = newColor;

    //creamos una variable max index para comprobar si
    //el vecino se sale de los limites.
    const maxIndex = image.length - 1;

    //creamos una cola con los vecinos
    let cola = getVecinos(sr, sc, maxIndex);

    for (let i = 0; i < cola.length; i++) {
        const coord = cola[i];
        const row = coord[0];
        const col = coord[1];

        if (image[row][col] === initColor) {
            image[row][col] = newColor;

            if (i < 4) {
                cola = cola.concat(getVecinos(row, col, maxIndex));
            }
        }

    }

    return image;
};

/**
 * Este metodo devuelve una array con todos las posiciones 'vecinas' de la posicion
 * que le pasamos (row y col) y que esten dentro de los limites de la imagen (0 >=< max).
 * @param {number} row 
 * @param {number} col 
 * @param {number} max 
 */
function getVecinos(row, col, max) {
    const vecinos = [[row, col - 1], [row, col + 1], [row - 1, col], [row + 1, col]];

    for (let index = 0; index < vecinos.length; index++) {
        const vecino = vecinos[index];

        if ((vecino[0] > max || vecino[0] < 0) || (vecino[1] > max || vecino[1] < 0)) {
            vecinos.splice(index, 1);
        }
    }

    return vecinos;
}

image = [[1, 1, 1,], [1, 1, 0], [1, 0, 1]]
let sr = 1, sc = 1, newColor = 2;

console.log('nueva imagen:', floodFill(image, sr, sc, newColor));

//otro ejemplo
image2 = [[1, 1, 0, 0, 1], [1, 0, 1, 0, 1], [0, 1, 1, 1, 0], [1, 1, 0, 0, 0], [1, 1, 1, 1, 1]]
let sr2 = 2, sc2 = 2, newColor2 = 2;

console.log('nueva imagen2:', floodFill(image2, sr2, sc2, newColor2));

