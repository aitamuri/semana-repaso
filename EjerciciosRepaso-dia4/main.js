
const array = [12, 2, 6, 7, 11];

function options(arrayOfSeats) {
    const totalSeats = arrayOfSeats.shift();
    const occupiedSeats = arrayOfSeats;

    const options = [];

    for (let seat = 1; seat <= totalSeats; seat++) {
        if (occupiedSeats.indexOf(seat) !== -1) {
            const asientoAbajo = Number(seat) + 2;
            if (asientoAbajo <= totalSeats && occupiedSeats.indexOf(asientoAbajo) !== -1) {
                options.push([seat, asientoAbajo]);
            }

            if (seat % 2 !== 0) {
                const asientoDerecha = Number(seat) + 1;
                if (occupiedSeats.indexOf(asientoDerecha) !== -1) {
                    options.push([seat, asientoDerecha]);
                }
            }
        }
    }

    return options;
}

console.log(options(array));