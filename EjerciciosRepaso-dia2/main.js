/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
    let bestProfit = 0;
    for (let i = 0; i < prices.length - 1; i++) {
        for (let j = i + 1; j < prices.length; j++) {
            const calc = (prices[j] - prices[i]);
            if (calc > bestProfit) {
                bestProfit = calc;
            }
        }
    }
    return bestProfit;
}

console.log('max profit:', maxProfit([7, 6, 2, 4, 3, 1]));

/**
 * @param {string} moves
 * @return {boolean}
 */
var judgeCircle = function (moves) {
    let x = 0;
    let y = 0;

    for (let move of moves) {
        switch (move) {
            case 'U':
                y++;
                break;
            case 'D':
                y--;
                break;
            case 'R':
                x++;
                break;
            case 'L':
                x--;
                break;
        }
    }

    if (x !== 0 || y !== 0) {
        return false;
    }

    return true;
};

console.log('robot returns to origin?', judgeCircle('UDRLUD'));

class Robot {
    constructor(space) {
        this.space = space;
        this.xPos = 0;
        this.yPos = 0;

        this.xSpaceLength = this.space[0].length;
        this.ySpaceLength = this.space.length;
    }

    moveLeft() {
        if (this.yPos > 0) {
            this.yPos--;
        }
    }

    moveRight() {
        if (this.yPos < this.xSpaceLength - 1) {
            this.yPos++;
        }
    }

    moveUp() {
        if (this.xPos > 0) {
            this.xPos--;
        }
    }

    moveDown() {
        if (this.xPos < this.ySpaceLength - 1) {
            this.xPos++;
        }
    }

    currentPosition() {
        return this.space[this.xPos][this.yPos];
    }
}

const mySpace = [[1, 9, 5, 8, 7, 6], [2, 3, 7, 2, 1, 9], [4, 8, 2, 1, 6, 5]];
const myRobot = new Robot(mySpace);

// primera parte ejercicio
console.log('moviendonos en el eje x:');
console.log('posición actual:', myRobot.currentPosition());

myRobot.moveRight();
console.log(myRobot.currentPosition());

myRobot.moveLeft();
console.log(myRobot.currentPosition());

myRobot.moveLeft();
console.log(myRobot.currentPosition());

myRobot.moveRight();
myRobot.moveRight();
myRobot.moveRight();
console.log(myRobot.currentPosition());

myRobot.moveRight();
myRobot.moveRight();
myRobot.moveRight();
console.log(myRobot.currentPosition());

// segunda parte ejercicio
console.log('moviendonos en el eje y:');
console.log('posición actual:', myRobot.currentPosition());

myRobot.moveUp();
console.log(myRobot.currentPosition());

myRobot.moveDown();
myRobot.moveDown();
myRobot.moveDown();
console.log(myRobot.currentPosition());

myRobot.moveUp();
console.log(myRobot.currentPosition());